package com.pragma.twilio_microservice.infrastructure.out.twilio.factory;

import com.pragma.twilio_microservice.domain.model.MessageModel;
import com.pragma.twilio_microservice.infrastructure.out.twilio.entity.MessageCodeEntity;

public class FactoryAdapterDataTest {
    public static MessageModel getMessageModel() {
        MessageModel messageModel = new MessageModel();
        messageModel.setId(1l);
        messageModel.setMessage("message");
        messageModel.setPhoneNumber("+573222333221");
        return messageModel;
    }

    public static MessageCodeEntity getMessageCodeEntity() {
        MessageCodeEntity messageCodeEntity = new MessageCodeEntity();
        messageCodeEntity.setId(1l);
        messageCodeEntity.setCode(123456);
        return messageCodeEntity;
    }
}
