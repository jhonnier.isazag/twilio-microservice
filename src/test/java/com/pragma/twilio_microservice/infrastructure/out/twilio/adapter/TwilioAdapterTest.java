package com.pragma.twilio_microservice.infrastructure.out.twilio.adapter;

import com.pragma.twilio_microservice.domain.model.MessageModel;
import com.pragma.twilio_microservice.infrastructure.out.twilio.entity.MessageCodeEntity;
import com.pragma.twilio_microservice.infrastructure.out.twilio.factory.FactoryAdapterDataTest;
import com.pragma.twilio_microservice.infrastructure.out.twilio.propierties.TwilioConfiguration;
import com.pragma.twilio_microservice.infrastructure.out.twilio.repository.IMessageCodeRepository;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockserver.client.MockServerClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@ExtendWith(SpringExtension.class)
class TwilioAdapterTest {

    @InjectMocks
    TwilioAdapter twilioAdapter;
    @Mock
    IMessageCodeRepository messageCodeRepository;
    @Mock
    TwilioConfiguration twilioConfiguration;
    @Mock
    private MockServerClient mockServerClient;
    @InjectMocks
    MessageCreator messageCreator;

    @Value("${twilio.trial_number}")
    private String twilioPhoneNumber;

    @Test
    void sendMessage() {

    }

    @Test
    void validateCode() {
        MessageCodeEntity messageCode = FactoryAdapterDataTest.getMessageCodeEntity();
        Mockito.when(messageCodeRepository.findById(anyLong())).thenReturn(Optional.of(messageCode));

        twilioAdapter.validateCode(123456, 1l);
        Mockito.verify(messageCodeRepository).delete(any(MessageCodeEntity.class));
    }

    @Test
    void throwCodeIdNotFoundExceptionWhenAttemptValidatingCodeWithIdBad() {
        Mockito.when(messageCodeRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> twilioAdapter.validateCode(123456, 1l));
    }

    @Test
    void throwCodeIdNotFoundExceptionWhenAttemptValidatingCodeWithCodeBad() {
        MessageCodeEntity messageCode = FactoryAdapterDataTest.getMessageCodeEntity();

        Mockito.when(messageCodeRepository.findById(anyLong())).thenReturn(Optional.of(messageCode));
        Mockito.when(messageCodeRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> twilioAdapter.validateCode(654321, 1l));
    }
}