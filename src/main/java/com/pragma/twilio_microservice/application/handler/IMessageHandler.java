package com.pragma.twilio_microservice.application.handler;

import com.pragma.twilio_microservice.application.dto.request.MessageRequestDto;

public interface IMessageHandler {
    void sendMessage(MessageRequestDto requestDto);
    void validateCode(Integer code, Long id);
}
