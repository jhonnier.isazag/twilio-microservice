package com.pragma.twilio_microservice.application.handler.impl;

import com.pragma.twilio_microservice.application.dto.request.MessageRequestDto;
import com.pragma.twilio_microservice.application.handler.IMessageHandler;
import com.pragma.twilio_microservice.application.mapper.IMessageRequestMapper;
import com.pragma.twilio_microservice.domain.api.IMessageServicePort;
import com.pragma.twilio_microservice.domain.model.MessageModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessageHandler implements IMessageHandler {
    private final IMessageServicePort messageServicePort;
    private final IMessageRequestMapper messageRequestMapper;

    @Override
    public void sendMessage(MessageRequestDto requestDto) {
        if(requestDto != null) {
            MessageModel message = messageRequestMapper.toMessage(requestDto);
            messageServicePort.sendMessage(message);
        }
    }

    @Override
    public void validateCode(Integer code, Long id) {
        messageServicePort.validateCode(code, id);
    }
}
