package com.pragma.twilio_microservice.application.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageRequestDto {
    private Long id;
    private String phoneNumber;
    private String message;
}
