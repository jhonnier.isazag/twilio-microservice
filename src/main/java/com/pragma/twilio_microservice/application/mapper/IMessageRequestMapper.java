package com.pragma.twilio_microservice.application.mapper;

import com.pragma.twilio_microservice.application.dto.request.MessageRequestDto;
import com.pragma.twilio_microservice.domain.model.MessageModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IMessageRequestMapper {
    MessageModel toMessage(MessageRequestDto requestDto);
}
