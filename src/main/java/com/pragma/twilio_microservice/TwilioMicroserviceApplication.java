package com.pragma.twilio_microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwilioMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwilioMicroserviceApplication.class, args);
	}

}
