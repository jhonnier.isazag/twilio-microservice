package com.pragma.twilio_microservice.domain.usecase;

import com.pragma.twilio_microservice.domain.api.IMessageServicePort;
import com.pragma.twilio_microservice.domain.exception.DomainException;
import com.pragma.twilio_microservice.domain.model.MessageModel;
import com.pragma.twilio_microservice.domain.spi.IMessagePersistencePort;

public class MessageUseCase implements IMessageServicePort {

    private final IMessagePersistencePort messagePersistencePort;

    public MessageUseCase(IMessagePersistencePort messagePersistencePort) {
        this.messagePersistencePort = messagePersistencePort;
    }

    @Override
    public void sendMessage(MessageModel message) {
        if(validateData(message) && validatePhone(message.getPhoneNumber())) messagePersistencePort.sendMessage(message);
    }

    @Override
    public void validateCode(Integer code, Long id) {
        messagePersistencePort.validateCode(code, id);
    }

    private boolean validateData(MessageModel message) {
        return message != null && message.getPhoneNumber() != null && message.getMessage() != null;
    }

    private boolean validatePhone(String phone) {
        String completePhone = "^\\+(\\d{12})$";
        if(phone.length() == 13 && phone.matches(completePhone)) {
            return true;
        } else {
            throw new DomainException("Phone number [" + phone + "] is not a valid number");
        }
    }
}
