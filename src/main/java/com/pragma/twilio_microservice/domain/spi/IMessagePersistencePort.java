package com.pragma.twilio_microservice.domain.spi;

import com.pragma.twilio_microservice.domain.model.MessageModel;

public interface IMessagePersistencePort {
    void sendMessage(MessageModel message);

    void validateCode(Integer code, Long id);
}
