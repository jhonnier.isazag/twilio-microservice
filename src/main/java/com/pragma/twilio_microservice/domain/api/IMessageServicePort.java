package com.pragma.twilio_microservice.domain.api;

import com.pragma.twilio_microservice.domain.model.MessageModel;

public interface IMessageServicePort {
    void sendMessage(MessageModel message);

    void validateCode(Integer code, Long id);
}
