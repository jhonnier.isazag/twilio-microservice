package com.pragma.twilio_microservice.infrastructure.out.twilio.repository;

import com.pragma.twilio_microservice.infrastructure.out.twilio.entity.MessageCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IMessageCodeRepository extends JpaRepository<MessageCodeEntity, Long> {
}
