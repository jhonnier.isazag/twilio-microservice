package com.pragma.twilio_microservice.infrastructure.out.twilio.adapter;

import com.pragma.twilio_microservice.domain.model.MessageModel;
import com.pragma.twilio_microservice.domain.spi.IMessagePersistencePort;
import com.pragma.twilio_microservice.infrastructure.exception.CodeIdNotFoundException;
import com.pragma.twilio_microservice.infrastructure.out.twilio.entity.MessageCodeEntity;
import com.pragma.twilio_microservice.infrastructure.out.twilio.propierties.TwilioConfiguration;
import com.pragma.twilio_microservice.infrastructure.out.twilio.repository.IMessageCodeRepository;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;

import java.util.Random;

@RequiredArgsConstructor
public class TwilioAdapter implements IMessagePersistencePort {

    private final IMessageCodeRepository messageCodeRepository;
    private final TwilioConfiguration twilioConfiguration;

    @Override
    public void sendMessage(MessageModel messageModel) {
        PhoneNumber to = new PhoneNumber(messageModel.getPhoneNumber());
        PhoneNumber from = new PhoneNumber(twilioConfiguration.getTrialNumber());
        Integer otp = generateOTP();
        String message = messageModel.getMessage() + ": " + otp;
        MessageCreator creator = Message.creator(
                to, from, message);
        creator.create();
        MessageCodeEntity code = new MessageCodeEntity(messageModel.getId(), otp);
        messageCodeRepository.save(code);
    }

    @Override
    public void validateCode(Integer code, Long id) {
        MessageCodeEntity messageCode = messageCodeRepository.findById(id).orElseThrow(
                () -> {
                    throw new CodeIdNotFoundException();
                }
        );
        if(messageCode.getCode().intValue() == code.intValue()) {
            messageCodeRepository.delete(messageCode);
        } else {
            throw new CodeIdNotFoundException();
        }
    }

    //6 digit otp
    private Integer generateOTP() {
        return new Random().nextInt(999999);
    }
}
