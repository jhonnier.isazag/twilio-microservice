package com.pragma.twilio_microservice.infrastructure.out.twilio.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "message_code")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MessageCodeEntity {

    @Id
    private Long id;
    @NotNull
    private Integer code;
}
