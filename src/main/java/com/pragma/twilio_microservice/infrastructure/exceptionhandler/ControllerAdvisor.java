package com.pragma.twilio_microservice.infrastructure.exceptionhandler;

import com.pragma.twilio_microservice.infrastructure.exception.CodeIdNotFoundException;
import com.pragma.twilio_microservice.infrastructure.exception.NoDataFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {

    private static final String MESSAGE = "message";

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoDataFoundException(
            NoDataFoundException ignoredNoDataFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.NO_DATA_FOUND.getMessage()));
    }
    @ExceptionHandler(CodeIdNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleCodeIdNotFoundException(
            CodeIdNotFoundException ignoredCodeIdNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.CODE_ID_NO_FOUND.getMessage()));
    }
    
}