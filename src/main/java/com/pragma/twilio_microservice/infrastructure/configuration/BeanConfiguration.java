package com.pragma.twilio_microservice.infrastructure.configuration;


import com.pragma.twilio_microservice.domain.api.IMessageServicePort;
import com.pragma.twilio_microservice.domain.spi.IMessagePersistencePort;
import com.pragma.twilio_microservice.domain.usecase.MessageUseCase;
import com.pragma.twilio_microservice.infrastructure.out.twilio.adapter.TwilioAdapter;
import com.pragma.twilio_microservice.infrastructure.out.twilio.propierties.TwilioConfiguration;
import com.pragma.twilio_microservice.infrastructure.out.twilio.repository.IMessageCodeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    private final TwilioConfiguration twilioConfiguration;
    private final IMessageCodeRepository messageCodeRepository;

    @Bean
    public IMessagePersistencePort messagePersistencePort() {
        return new TwilioAdapter(messageCodeRepository, twilioConfiguration);
    }

    @Bean
    public IMessageServicePort messageServicePort() {
        return new MessageUseCase(messagePersistencePort());
    }
}