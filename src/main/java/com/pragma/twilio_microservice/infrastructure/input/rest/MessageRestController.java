package com.pragma.twilio_microservice.infrastructure.input.rest;

import com.pragma.twilio_microservice.application.dto.request.MessageRequestDto;
import com.pragma.twilio_microservice.application.handler.IMessageHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/message")
@RequiredArgsConstructor
public class MessageRestController {

    private final IMessageHandler messageHandler;

    @PostMapping("/")
    public ResponseEntity<Void> sendMessage(@RequestBody MessageRequestDto requestDto) {
        messageHandler.sendMessage(requestDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/validate-code/{code}/{id}")
    public ResponseEntity<Void> validateCode(@PathVariable Integer code, @PathVariable Long id) {
        messageHandler.validateCode(code, id);
        return ResponseEntity.ok().build();
    }

}
