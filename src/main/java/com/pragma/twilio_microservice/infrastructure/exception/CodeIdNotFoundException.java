package com.pragma.twilio_microservice.infrastructure.exception;

public class CodeIdNotFoundException extends RuntimeException {
    public CodeIdNotFoundException() {
        super();
    }
}
