package com.pragma.twilio_microservice.infrastructure.exception;

public class NoDataFoundException extends RuntimeException {
    public NoDataFoundException() {
        super();
    }
}
